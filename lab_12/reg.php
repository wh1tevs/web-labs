<?php
    $LOGIN = $_POST['login'];
    $PWD = $_POST['password'];    
    $PWD_C = $_POST['confirm_password'];
    $USERS_DIR = "./users/";

    if (!file_exists($USERS_DIR.$LOGIN)) {
        if ($PWD == $PWD_C) {            
            $data = fopen($USERS_DIR.$LOGIN, "a");
            fwrite($data, $PWD);
            fclose($data);
            echo 'Вы успешно зарегистрировались! <a href="./login.html">Войти</a>';
        } else {
            echo 'Пароли не совпадают';
        }
    } else {
        echo 'Пользователь с таким логином уже существует';
    }
?>