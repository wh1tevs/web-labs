<?php
    $LOGIN = $_POST['login'];
    $PWD = $_POST['password'];
    $USERS_DIR = "./users/";

    if (file_exists($USERS_DIR.$LOGIN)) {
        $data = file($USERS_DIR.$LOGIN);
        if ($PWD == $data[0]) {
            echo 'Вы успешно авторизированы';
        } else {
            echo 'Неверный пароль';
        }
    } else {
        echo 'Пользователя не существует';
    }
?>