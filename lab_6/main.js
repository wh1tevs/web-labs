
var main_records = getInitialRecords();
var view_records = main_records;

function time(date) {
    return "" + date.toLocaleTimeString();
}

function day(date) {
    return "" + date.toLocaleDateString();
}

function Record(text, date) {
    this.text = text;
    this.date = date;
    this.time = time(date);
    this.day = day(date);
}

function displayRecords() {
    var text = "";
    var odd = true;
    for (var record in view_records)
    {
        if (odd)
            text += "<p style='background-color:gray'>";
        else
            text += "<p style='background-color:lightgray'>";
        odd = !odd;
        text += view_records[record].time + " ";
        text += "<strong>" + view_records[record].day + "</strong><br>";
        text += view_records[record].text + "</p>";
    }
    document.getElementById("records_place").innerHTML = text;
}

function send() {
    var record = new Record(document.getElementById("newtext").value, new Date());
    main_records.push(record);    
    document.getElementById("newtext").value = ""; 
    displayRecords();
}

function sort(compare) {
    var l = view_records.length;

    for (var i = 0; i < l-1; i++)
    {
        var j_min = i;
        for (var j = i + 1; j < l; j++)
        {
            if (compare(view_records[j], view_records[j_min]))
                j_min = j;
        }
        var r = view_records[j_min];
        view_records[j_min] = view_records[i];
        view_records[i] = r;
    }
    displayRecords()
}

function findAll () {
    var search_text = document.getElementById("newtext").value;
    var records = []
    view_records.forEach(record => {
        if (record.text.includes(search_text)) {
            records.push(record);
        }
    });
    view_records = records;
    displayRecords();
}

function getMorningRecords() {
    var records = [];
    view_records.forEach(record => {
        if(record.date.getHours() >= 6 && record.date.getHours() <= 10) {
            records.push(record);
        }
    });
    view_records = records;
    displayRecords();
}

function getInitialRecords() {
    var records = [
        new Record("Я проснулся!",
            new Date(2013, 10, 19, 07, 00, 0 ,0)),
        new Record("Я умываюсь...",
            new Date(2013, 10, 19, 07, 10, 0, 0)),
        new Record("Пошел варить кофе",
            new Date(2013, 10, 19, 5, 20, 0, 0)),
        new Record("Куда опять ключи подевались?",
            new Date(2013, 10, 19, 11, 50, 0, 0)),
        new Record("Опять опаздываю",
            new Date(2013, 10, 19, 20, 05, 0, 0))
    ];

    return records;
}

function resetRecords() {
    view_records = main_records;
    displayRecords();
}

var comparers = [
        function comp1(r1, r2) {
            return r1.date < r2.date;
        },

        function comp2(r1, r2) {
            return r1.date > r2.date;
        },

        function comp3(r1, r2) {
            return r1.text < r2.text;
        },

        function comp4(r1, r2) {
            return r1.text > r2.text;
        }
    ];