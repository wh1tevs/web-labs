<!DOCTYPE html>
<html>
<head>
    <?php 
        $mysqli = new mysqli("localhost", "root", "", "mydb");
        $mysqli->set_charset("utf8");
    ?>
    <meta charset="UTF-8">
    <title>Search by author</title>
</head>
<body>
    <form action="#" method="get">
        <div>Автор: <input type="text" name="author">
        <input type="submit" value="Найти"></div>
    </form>

    <table>
        <?php
           if(!$_GET || $_GET['author'] == "#") {
                echo "<tr><td><strong>Авторы</strong></td></tr>";
                $command = "select distinct `author` from `books`";
                $result = $mysqli->query($command);
                while($row = mysqli_fetch_array($result)) {
                    echo <<<EOT
                    <tr>
                        <td>{$row['author']}</td>
                    </tr>
EOT;
                }
           } else {
                $AUTHOR = $_GET['author'];
                $command = "select";
                echo "<tr><td><strong>Название книги</strong></td><td><strong>Авторы</strong></td></tr>";
                $command = "select `title`, `author` from `books` where `author` = '$AUTHOR'";
                $result = $mysqli->query($command);
                while($row = mysqli_fetch_array($result)) {
                    echo <<<EOT
                    <tr>
                        <td>{$row['title']}</td>
                        <td>{$row['author']}</td>
                    </tr>
EOT;
                }
           }
        ?>
    </table>
    
</body>
</html>