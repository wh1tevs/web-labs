<%@ page import="java.util.*"%>

<%  Thread.sleep(5000);
    String tz = request.getParameter("timezone");
    if (tz == null)
    {
        tz = TimeZone.getAvailableIDs()[0];
    }
    Calendar c = Calendar.getInstance(TimeZone.getTimeZone(tz));
    int hs = c.get(Calendar.HOUR);
    int ms = c.get(Calendar.MINUTE);
    int ss = c.get(Calendar.SECOND);
%>
    <html>
        <head><title>Current Time</title><head>

        <body>
            <strong>The current time is:</strong>
            <span style="color:red">
                <% out.println(hs + " h " + ms + " m " + ss + " s "); %>
            </span>
            <br>
            <form action="w_9.jsp" method="get">
                <select name="timezone">
                <%
                for (String x : TimeZone.getAvailableIDs())
                {
                    if (x.equals(tz))
                    {
                        out.println("<option selected value=\'"  + x +"\'>" + x + "</option>");
                    } else
                    {
                        out.println("<option value=\'"  + x +"\'>" + x + "</option>");
                    }
                }
                %>
                </select>
                <input type="submit" value="Update">
            </form>
        </body>
    </html>