<%@ page import="java.util.*"%>

<%

    Thread.sleep(5000);
    String tz = request.getParameter("t");
    if (tz == null)
    {
        tz = TimeZone.getAvailableIDs()[0];
    }
    Calendar c = Calendar.getInstance(TimeZone.getTimeZone(tz));
    int hs = c.get(Calendar.HOUR);
    int ms = c.get(Calendar.MINUTE);
    int ss = c.get(Calendar.SECOND);
    out.println(hs + " hours " + ms + " minutes " + ss + " seconds");
%>