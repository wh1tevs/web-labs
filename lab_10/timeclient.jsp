<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Current Time</title>
    <script>
        function getTime()
        {
            var request;
            if (window.XMLHttpRequest)
            {
                request=new XMLHttpRequest();
            } else
            {
                request=new ActiveXObject("Microsoft.XMLHTTP");
            }
            request.onreadystatechange = function ()
            {
                if (request.readyState==4 && request.status==200)
                {
                    document.getElementById("timeplace").innerHTML=request.responseText;
                }
            };
            var ind=document.getElementById("timezones").selectedIndex;
            var ops=document.getElementById("timezones").options;
            request.open("POST", "time_ajax.jsp?t="+ops[ind].value, true);
            request.send();
        }
    </script>
</head>

<body onload="getTime()">
    <strong>The current time is:</strong>
    <span id="timeplace" style="color:red"></span>
    <br>
    <select id="timezones" name="timezone">
        <%
        for (String x : java.util.TimeZone.getAvailableIDs())
        {
            out.println("<option value=\'"  + x +"\'>" + x + "</option>");
        }
        %>
    </select>
    <input type="button" value="Update" onclick="getTime()">
</body>
</html>